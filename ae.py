import numpy as np 
import pandas as import pd 
import torch
import torch.nn as nn 
import torch.nn.parallel
import torch.optim as optim 
import torch.utils.data 
from torch.autograd import Variable

class autoEncoder(nn.Module):
    '''
    It's used as recomender system
    Category: Hibryd (self Supervionate and Unsupervised)
    '''
    def __init__(self, training_set, test_set):

        super(SAE, self).__init__()
        self.fc1 = nn.Linear(nb_movies, 20)
        self.fc2 = nn.Linear(20, 10)
        self.fc3 = nn.Linear(10, 20)
        self.fc4 = nn.Linear(20, nb_movies)
        self.activation = nn.Sigmoid()
        self.training_set = training_set 
        self.test_set = test_set
        self.nb_epoch = 200

        # Getting the number of users and film to apply cross_validation
        self.nb_users = int(max(max(training_set[:,0]), max(test_set[:,0])))
        self.nb_movies = int(max(max(training_set[:,1]), max(test_set[:,1])))

        
        self.criterion = nn.MSELoss()
        self.optimizer = optim.RMSprop(self.parameters(), 
                                        lr = 0.01, weight_decay = 0.5)

    def convert(self):

        new_data = []
        
        for id_users in range(1, nb_users + 1):
            id_movies = data[:,1][data[:,0] == id_users]
            id_ratings = data[:,2][data[:,0] == id_users]
            ratings = np.zeros(nb_movies)
            ratings[id_movies - 1] = id_ratings
            new_data.append(list(ratings))
        
        return new_data

    def forward(self, x):

        x = self.activation(self.fc1(x))
        x = self.activation(self.fc2(x))
        x = self.activation(self.fc3(x))
        x = self.fc4(x)
        return x

    def train_autoencoder(self):

        for epoch in range(1, self.nb_epoch + 1):
            train_loss = 0
            s = 0.
            for id_user in range(self.nb_users):
                input = Variable(self.training_set[id_user]).unsqueeze(0)
                target = input.clone()
                if torch.sum(target.data > 0) > 0:
                    output = sae(input)
                    target.require_grad = False
                    output[target == 0] = 0
                    loss = self.criterion(output, target)
                    mean_corrector = self.nb_movies/float(torch.sum(target.data > 0) + 1e-10)
                    loss.backward()
                    train_loss += np.sqrt(loss.data[0]*mean_corrector)
                    s += 1.
                    self.optimizer.step()
            print('epoch: '+str(epoch)+' loss: '+str(train_loss/s))

    def test_autoenconder(self):

        test_loss = 0
        s = 0.
        for id_user in range(self.nb_users):
            input = Variable(self.training_set[id_user]).unsqueeze(0)
            target = Variable(self.test_set[id_user])
            if torch.sum(target.data > 0) > 0:
                output = sae(input)
                target.require_grad = False
                output[target == 0] = 0
                loss = self.criterion(output, target)
                mean_corrector = self.nb_movies/float(torch.sum(target.data > 0) + 1e-10)
                test_loss += np.sqrt(loss.data[0]*mean_corrector)
                s += 1.
        print('test loss: '+str(test_loss/s))

    def execute(self):

        self.convert()
        self.train_autoencoder()
        self.test_autoenconder()


if __name__ == '__main__':


    # Importing the dataset
    movies = pd.read_csv('ml-1m/movies.dat', sep = '::', header = None, engine = 'python', encoding = 'latin-1')
    users = pd.read_csv('ml-1m/users.dat', sep = '::', header = None, engine = 'python', encoding = 'latin-1')
    ratings = pd.read_csv('ml-1m/ratings.dat', sep = '::', header = None, engine = 'python', encoding = 'latin-1')

    # Preparing the training set and the test set
    training_set = pd.read_csv('ml-100k/u1.base', delimiter = '\t')
    training_set = np.array(training_set, dtype = 'int')
    test_set = pd.read_csv('ml-100k/u1.test', delimiter = '\t')
    test_set = np.array(test_set, dtype = 'int')

    ae = autoEncoder(training_set, test_set)
    ae.execute()